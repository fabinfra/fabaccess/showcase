{- Main configuration file for bffh
 - ================================
 -
 - In this configuration file you configure almost all parts of how bffh operates, but most importantly:
 -      * Machines
 -      * Initiators and Actors
 -      * Which Initiators and Actors relate to which machine(s)
 -      * Roles and the permissions granted by them
 -}

-- The config is in the configuration format/language dhall. You can find more information about dhall over at
-- https://dhall-lang.org

-- (Our) Dhall is somewhat similar to JSON and YAML in that it expects a top-level object containing the
-- configuration values
{
    -- Configure the addresses and ports bffh listens on
    listens = [
        -- BFFH binds a port for every listen object in this array.
        -- Each listen object is of the format { address = <STRING>, port = <INTEGER> }
        -- If you don't specify a port bffh will use the default of `59661`
        -- 'address' can be a IP address or a hostname
        -- If bffh can not bind a port for the specified combination if will log an error but *continue with the remaining ports*
        { address = "::", port = Some 59661 }
    ],

    -- Configure TLS. BFFH requires a PEM-encoded certificate and the associated key as two separate files
    certfile = "/etc/letsencrypt/cert.pem",
    keyfile = "/etc/letsencrypt/key.pem",

    -- BFFH right now requires a running MQTT broker.
    mqtt_url = "tcp://mqtt:1883", 

    -- Path to the database file for bffh. bffh will in fact create two files; ${db_path} and ${db_path}.lock.
    -- BFFH will *not* create any directories so ensure that the directory exists and the user running bffh has write
    -- access into them.
    db_path = "/var/lib/bffh/db",

    -- Audit log path. Bffh will log state changes into this file, one per line.
    -- Audit log entries are for now JSON:
    -- {"timestamp":1641497361,"machine":"Testmachine","state":{"state":{"InUse":{"uid":"Testuser","subuid":null,"realm":null}}}}
    auditlog_path = "/tmp/bffh.audit",

    -- In dhall you can also easily import definitions from other files, e.g. you could write
    -- roles = ./roles.dhall
    










    roles = {
        Admin = {
            permissions =  [
                "TestEnv.Admin",
                "TestEnv.Manage",
                "TestEnv.Write",
                "TestEnv.Read",
                "TestEnv.Disclose",
            ]
        },

        ManageUsers = {
            permissions = [ 
                "bffh.users.info", 
                "bffh.users.manage", 
                "bffh.users.admin" 
            ]
        },

        Manage = {
            permissions = [ "TestEnv.Manage" ]
        },

        Use = {
            permissions = [ "TestEnv.Write" ]
        },

        Read = {
            permissions = [ "TestEnv.Read" ]
        },
        
        Disclose = {
            permissions = [ "TestEnv.Disclose" ]
        },
    },

    machines = {
        MachineB1 = {
            name = "Schließfach",
            description = "Schließfach ohne elektrische Steuerung",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryB",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },
        MachineB2 = {
            name = "Fabulaser",
            description = "Fabulaser - compact, yet powerful",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryB",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },

        MachineA1 = {
            name = "Machine Oben",
            description = "Maschine mit QR-Code zum scannen",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryA",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },
        MachineA2 = {
            name = "Machine Mitte",
            description = "Maschine mit NFC Reader für DESFire Karten",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryA",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },
        MachineA3 = {
            name = "Machine Unten",
            description = "Maschine für weitere Dinge",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryA",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },
        MachineA4 = {
            name = "Machine Drehstrom",
            description = "Maschine mit Drehstromanschluss",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryA",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },
        MachineA5 = {
            name = "Machine Drehstrom Anlaufschutz",
            description = "Maschine mit Drehstromanschluss und Wiederanlaufschutz",
            wiki = "https://fab-access.readthedocs.io",
            category = "CategoryA",
            
            disclose = "TestEnv.Disclose",
            read = "TestEnv.Read",
            write = "TestEnv.Write",
            manage = "TestEnv.Manage"
        },
    },
    
    actors = {
        -- Actors similarly to machines have an 'id'. This id (here "Shelly1234") is limited to Alphanumeric ASCII
        -- and must begin with a letter.
        shellyplug-s-6E6ED9 = { module = "Shelly", params = {=} },
        shellyplug-s-C18903 = { module = "Shelly", params = {=} },
        shellyplug-s-B4C8B9 = { module = "Shelly", params = {=} },
        shelly1-DDDDDDDDDDDD = { module = "Shelly", params = {=} },
        shelly1-EEEEEEEEEEEE = { module = "Shelly", params = {=} },
    },

    actor_connections = [
        { machine = "MachineA1", actor = "shellyplug-s-6E6ED9" },
        { machine = "MachineA2", actor = "shellyplug-s-C18903" },
        { machine = "MachineA3", actor = "shellyplug-s-B4C8B9" },
        { machine = "MachineA4", actor = "shelly1-DDDDDDDDDDDD" },
        { machine = "MachineA5", actor = "shelly1-DDDDDDDDDDDD" },
    ],

    initiators = {=},

    init_connections = [] : List { machine : Text, initiator : Text },
}
